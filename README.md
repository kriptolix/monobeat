# monobeat

Study project that aims to refactor the Monophony code, adding a new graphical interface and migrating the interface construction from pure pythom to python+gtk xml.

The original app can be found here https://gitlab.com/zehkira/monophony
