from gi.repository import Gtk
from gi.repository import GObject
from gi.repository import Gio

@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/RowMenu.ui')
class RowMenu(Gtk.PopoverMenu):
    __gtype_name__ = 'RowMenu'

    def __init__(self):
                
        super().__init__()     
