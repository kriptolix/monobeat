from gi.repository import Adw
from gi.repository import Gtk

@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/ConfirmDialog.ui')
class ConfirmDialog(Adw.MessageDialog):
    
    __gtype_name__ = 'ConfirmDialog'    

    def __init__(self):
                
        super().__init__()
        
        print("ConfirmDialog")
    
    def _quit(self, button):
        
        self.destroy()