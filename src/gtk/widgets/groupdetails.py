from gi.repository import Adw
from gi.repository import Gtk
from gi.repository import GObject

from .rowmenu import RowMenu


@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/GroupDetails.ui')
class GroupDetails(Gtk.Box):
    
    __gtype_name__ = 'GroupDetails'        
     
    app = GObject.Property(type=GObject.GObject, default=None)

    _list_container = Gtk.Template.Child()
    _menu_button = Gtk.Template.Child()
    _play_button = Gtk.Template.Child() # Play all group
    _songs_count = Gtk.Template.Child()
    _group_name = Gtk.Template.Child()

    def __init__(self, handler, title, songs):        
        
        super().__init__()        
        
        self._handler = handler

        self._group_name.props.label = title
        self._songs_count.props.label = songs
        self._menu_button = RowMenu() 

        self._list_container.connect("row-activated", self._handler._on_row_played)
        self._list_container.connect("row-activated", 
                                     self._handler._set_row_as_playing)
        
    def _insert_a_row(self, row):             
        
        self._list_container.append(row)

    
    