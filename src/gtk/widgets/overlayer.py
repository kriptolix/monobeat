from gi.repository import Gtk
from gi.repository import Adw
from gi.repository import GObject

@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/Overlayer.ui')
class Overlayer(Gtk.ActionBar):
    __gtype_name__ = 'Overlayer'

    _item_title = Gtk.Template.Child()
    _secondary_info = Gtk.Template.Child()
    _backward = Gtk.Template.Child()
    _playpause = Gtk.Template.Child()
    _forward = Gtk.Template.Child()
    _album_cover = Gtk.Template.Child()
    _duration = Gtk.Template.Child()
    _elapsed = Gtk.Template.Child()
    _progress = Gtk.Template.Child()

    last_row = GObject.Property(type=GObject.GObject, default=None)  

    def __init__(self):        
        
        super().__init__()            

        self._album_cover.set_resource('/io/gitlab/kriptolix/Monobeat/data/images/placeholder_album.jpeg')


    def _set_music_data(self, _item_title, _secondary_info, _duration, _album_cover):

        self._item_title.props.label = _item_title
        self._secondary_info.props.label = _secondary_info       
        self._duration.props.label = _duration       
        self._album_cover.set_resource('/io/gitlab/kriptolix/Monobeat/data/images/placeholder_album.jpeg')
           
    
    def _on_playpause_pressed():
        
        ''
    
    def _on_backward_pressed():
        
        ''
    
    def _on_forward_pressed():
        
        ''
    
    def _update_elapsed():

        ''

    def _update_progress():

        ''

    def _on_progress_tracking():

        '' 

    def _set_row_as_playing(self, klass, row):

        row.props.playing = True
        self.props.last_row = row

        if (self.props.last_row):
        
            self.props.last_row.props.playing = False
    
    def _on_row_played(self, klass, param):           
        
        ''' 
        When any music/video rows was activated the signal 
        is connected with this function
        '''
        self.props.revealed = True
        #self._overlayer._set_music_data(self, _item_title, _secondary_info, _duration, _album_cover)

        #print("row activated")