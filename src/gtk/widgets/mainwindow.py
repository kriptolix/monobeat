# window.py
#
# Copyright 2023 k
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk
from gi.repository import GObject
from gi.repository import Gio

from .appmenu import AppMenu
from .overlayer import Overlayer
from .resultspage import ResultsPage
from .statuspage import StatusPage
from .addtoplaylist import AddToPlaylist
from .confirmdialog import ConfirmDialog


@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/MainWindow.ui')
class MonobeatWindow(Adw.ApplicationWindow):
   
    __gtype_name__ = 'MonobeatWindow'
    

    current_search_view = GObject.Property(type=GObject.GObject, default=None)
    current_library_view = GObject.Property(type=GObject.GObject, default=None)
    last_section_viewed = GObject.Property(type=GObject.GObject, default=None)

    _overlay = Gtk.Template.Child()
    _overlayer = Gtk.Template.Child()
    _headerbar_stack = Gtk.Template.Child()
    _menu_button = Gtk.Template.Child()
    _search_entry = Gtk.Template.Child()   
    _search_results = Gtk.Template.Child()
    _search_status = Gtk.Template.Child()
    _search_details = Gtk.Template.Child()
    _library_results = Gtk.Template.Child()
    _library_status = Gtk.Template.Child()
    _library_details = Gtk.Template.Child()
    _back_button = Gtk.Template.Child()
    _search_button = Gtk.Template.Child()
    _library_button = Gtk.Template.Child()
    _test_button1 = Gtk.Template.Child()
    _test_button2 = Gtk.Template.Child()
    _switcher_box = Gtk.Template.Child()
    _revealer = Gtk.Template.Child()

    def __init__(self, **kwargs):        

        super().__init__(**kwargs)
        
        self._setup_sections()        

        self._menu_button.props.popover = AppMenu()

        self._playlist_add = AddToPlaylist 
        
        self._headerbar_stack.props.visible_child = self._search_status
        self._search_button.add_css_class("raised")
       
        self.props.current_search_view = self._search_status
        self.props.current_library_view = self._library_status
        
        #self._headerbar_stack.connect(
        #    "notify::visible-child-name", self._on_stack_visible_child_changed)
        
        self._test_button1.connect("clicked", self._on_test_button1_clicked)
        self._test_button2.connect("clicked", self._on_test_button2_clicked)
        self._back_button.connect("clicked", self._on_back_button_clicked)

        self._search_button.connect("clicked", self._on_search_tab_clicked)
        self._library_button.connect("clicked", self._on_library_tab_clicked)

        
    def _on_search_tab_clicked(self, button):

        if (self._library_button.has_css_class("raised")):
            self._library_button.remove_css_class("raised")

        if not (self._search_button.has_css_class("raised")):
            self._search_button.add_css_class("raised")                   
        
        self._revealer.props.reveal_child = True 
        self._headerbar_stack.props.visible_child = self.props.current_search_view
     
    def _on_library_tab_clicked(self, button):
            
        if (self._search_button.has_css_class("raised")):
            self._search_button.remove_css_class("raised")

        if not (self._library_button.has_css_class("raised")):
            self._library_button.add_css_class("raised")
        

        self._headerbar_stack.props.visible_child = self.props.current_library_view
        self._revealer.props.reveal_child = False    

    def _change_overlay_visible(self):

        if (self._overlay.props.revealed == True):
            self._overlay.props.revealed = False
        else:
            self._overlay.props.revealed = True
    
    def _setup_sections(self):

        self._search_results.props.window = self
        self._search_details.props.window = self
        
        self._search_results.props.name = "search_results"
        self._search_details.props.name = "search_details"
        self._search_status.props.name = "search_status" 

        self._library_results.props.window = self
        self._library_details.props.window = self
        
        self._library_results.props.name = "library_results"
        self._library_details.props.name = "library_details"
        self._library_status.props.name = "library_status"      

    def _on_see_button_clicked(self, button):

        ''' 
        When > button on playlists/albuns/artists rows was clicked
        the signal is connected with this function
        '''
        self.props.last_section_viewed = self._headerbar_stack.props.visible_child

        self._switcher_box.props.visible = False
        self._search_entry.props.visible = False
        self._back_button.props.visible = True

        self._headerbar_stack.props.transition_type = 3
        
        if (self._headerbar_stack.props.visible_child == self._search_results):
            self._headerbar_stack.props.visible_child = self._search_details

        if (self._headerbar_stack.props.visible_child == self._library_results):
            self._headerbar_stack.props.visible_child = self._library_details
   
    def _on_back_button_clicked(self, button):

        ''' 
        When < button on headbar was clicked
        the signal is connected with this function
        '''

        self._switcher_box.props.visible = True
        self._search_entry.props.visible = True
        self._back_button.props.visible = False

        self._headerbar_stack.props.transition_type = 2
        self._headerbar_stack.props.visible_child = self.props.last_section_viewed
        self._headerbar_stack.props.transition_type = 1

    def _on_add_to_playlist(self, list, row):
        
        self.add_window = self._playlist_add(row, self)
        self.add_window.present()
    
    def _on_remove_from_playlist(self, list, row):
        ''
    
    def _on_rename_playlist():
        ''

    def _on_exclude_playlist():
        ''

    def _on_test_button1_clicked(self, button):

        match self._headerbar_stack.props.visible_child:
            
            case self._search_status:                
                self._headerbar_stack.props.visible_child = self._search_results
                self.props.current_search_view = self._search_results                
            
            case self._search_results:
                self._headerbar_stack.props.visible_child = self._search_status
                self.props.current_search_view = self._search_status
            
            case self._library_status:
                self._headerbar_stack.props.visible_child = self._library_results
                self.props.current_library_view = self._library_results
           
            case self._library_results:
                self._headerbar_stack.props.visible_child = self._library_status
                self.props.current_library_view = self._library_status

    def _on_test_button2_clicked(self, button):

        self._new_window = ConfirmDialog()
        self._new_window.present()
        

             
