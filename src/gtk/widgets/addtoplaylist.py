from gi.repository import Gtk
from gi.repository import Adw

from .resultsrow import ResultsRow
from .createrename import CreateRename


@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/AddToPlaylist.ui')
class AddToPlaylist(Adw.Window):
    
    __gtype_name__ = 'AddToPlaylist'

    _new_playlist_button = Gtk.Template.Child()
    _playlists_list = Gtk.Template.Child()
    _cancel_button = Gtk.Template.Child()
    _add_button = Gtk.Template.Child()

    def __init__(self, item_to_add, window):
                
        super().__init__()

        self._window = window 
        
        self.props.name = "add_to_playlist"
        self.props.transient_for = self._window

        self._crete_row = ResultsRow
        self._create_playlist = CreateRename

        self._item = item_to_add
        
        self._new_playlist_button.connect("clicked", self._new_playlist)
        self._cancel_button.connect("clicked", self._quit)
        self._add_button.connect("clicked", self._add_to_playlist)
       

    def _new_playlist(self, button):
         
       self._name_window = self._create_playlist(self._window, self, "Create", 
                                                 "Playlist Name", "Create Playlist")
       self._name_window.present()


    def on_check_button_toggled(self, klass, row):

        '''
            When the checkbutton of a item-playlist is toggled, add/remove playlist
            name from array of selected 
        '''
        self._playlists_selected = []

        self._row = row
        self._check_status = self._row._check_button.props.active
        self._playlists = self._row._playlists.label

        if (self._check_status):
            
            if (self._playlists) not in (self._playlists_selected):
                self._playlists_selected.append(self._playlists)
        
        if not (self._check_status):

            if (self._playlists) in (self._playlists_selected):
                self._playlists_selected.remove(self._playlists)
        
        #return self._playlists_selected

    def _add_to_playlist(self, button):
        ''

    def _insert_a_row(self, item_title):
                
        self._row = self._crete_row(5, self, "0001", item_title, "", "", "")        
        self._playlists_list.append(self._row)
    
    def _quit(self, button):
        
        self.destroy()
    
    def _populate_playlists_list(self):

        '''
        Mockup content
        '''

        self._insert_a_row(5, self,"0001", "80's Rock", "", "47 songs", "1:09:56")
        self._insert_a_row(5, self,"0002", "Party Play Time", "", "39 songs", "57:31" )
        self._insert_a_row(5, self,"0001", "Rain and Grey Days", "", "82 songs", "1:47:28" )
        
        '''
        Mockup content
        '''
    
    