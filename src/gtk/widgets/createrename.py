from gi.repository import Adw
from gi.repository import Gtk

@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/CreateRename.ui')
class CreateRename(Adw.Window):
    
    __gtype_name__ = 'CreateRename'

    _name_entry = Gtk.Template.Child()
    _cancel_button = Gtk.Template.Child()
    _action_button = Gtk.Template.Child()
    _window_title = Gtk.Template.Child()
    _action_label = Gtk.Template.Child()

    def __init__(self, window, handler, action, label, title):
                
        super().__init__()

        self.props.transient_for = window
        self._action_button.props.label = action
        self._action_label.props.label = label
        self._window_title.props.title = title

        self._handler = handler

        self._cancel_button.connect("clicked", self._quit)
        self._action_button.connect("clicked", self._butto_action)
    
    def _butto_action(self, button):
        if (self._action_button.props.label == "Create"):
            self._create_playlist()
        
        if (self._action_button.props.label == "Rename"):
            self._rename_playlist()
    
    def _create_playlist(self):
        
        '''Just Frontend Part
        '''

        self._text = self._name_entry.get_text()
        self._handler._insert_a_row(self._text)
        #print(self._text)

        self.destroy()       
        
    
    def _rename_playlist():
        ''

    def _quit(self, button):
        
        self.destroy()