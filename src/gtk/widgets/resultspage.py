from gi.repository import Adw
from gi.repository import Gtk
from gi.repository import GObject

from .groupdetails import GroupDetails
from .resultsgroup import ResultsGroup
from .resultsrow import ResultsRow
from .addtoplaylist import AddToPlaylist

@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/ResultsPage.ui')
class ResultsPage(Gtk.Box):
    
    __gtype_name__ = 'ResultsPage'

    window = GObject.Property(type=GObject.GObject, default=None)    
    
    _container = Gtk.Template.Child()
    

    def __init__(self): # Type: Search Results or Playlists Results       
        
        super().__init__()

        self._window = None
        self._overlayer = None

        self._create_row = ResultsRow
        self._create_group_details = GroupDetails
        self._create_results_group = ResultsGroup
        self._add_to_playlist = AddToPlaylist
               
        #self._insert_group_end_row()
        
        self.connect("notify::window", self._on_main_page_load)
        self.connect("notify::name", self._test_insert_group_end_row) # Just to text       


    def _set_group(self, title, more, playable, handler):
        
        self._group = self._create_results_group(title, more, playable, handler)                
        self._container.append(self._group)       

        return self._group              

    def _set_details(self, handler, title, songs):

        self._details = self._create_group_details(handler, title, songs)
        self._container.append(self._details)

        return self._details

    def _set_row(self, mode, handler, reference, item_title, 
                       secondary_info, tertiary_info, duration):   
             
        self._row = self._create_row(mode, handler, reference, item_title, 
                                     secondary_info, tertiary_info, duration)
        
        return self._row    
    
    def _on_main_page_load(self, klass, param):
        
        self._window = self.props.window
        self._overlayer = self._window._overlayer    
    
    def _test_insert_group_end_row(self, klass, param):
        
        print(self.props.name)

        if (self.props.name == "search_results"):
            
            # Top Results

            self._top_group = self._set_group("Top Result", False, True, self._overlayer)
            self._a_row = self._set_row(0, self._window, "UCmMUZbaYdNH0b", 
                                        "Sweet child of mine", "Guns N' Roses" ,
                                        "80's rock", "3:56")
            self._top_group._insert_a_row(self._a_row)

            # Musics

            self._music_group = self._set_group("Musics", True, True, self._overlayer)
            self._a_row = self._set_row(0, self._window, "UCmMUZbaYdNH0b", 
                                        "Sweet child of mine", "Guns N' Roses" ,
                                        "80's rock", "3:56")
            
            self._music_group._insert_a_row(self._a_row)
            
            self._a_row = self._set_row(0, self._window, "FGghghghessdE", 
                                        "Com açucar, com afeto", "Chico Buarque" , 
                                        "MPB", "3:56")
            
            self._music_group._insert_a_row(self._a_row) 

            # Playlists
             
            self._playlist_group = self._set_group("Playlists", True, False, 
                                                   self._overlayer)
            self._a_row = self._set_row(1, self._window, "FGghghghessdE", 
                                        "Best of MPB" , "52 songs", "", "1:42:34")
            
            self._playlist_group._insert_a_row(self._a_row)
            #self._playlist_group._insert_a_row(self._a_row)
            #self._playlist_group._insert_a_row(self._a_row)

            # Artist
            
            self._artist_group = self._set_group("Playlists", True, False, 
                                                   self._overlayer)
            self._a_row = self._set_row(2, self._window, "FGghghghessdE", 
                                        "Chico Buarque" , "", "","")         
            
            self._artist_group._insert_a_row(self._a_row)

        if (self.props.name == "library_results"):
            
            self._top_group = self._set_group("Your Playlists", False, False, self._overlayer)
            self._a_row = self._set_row(4, self._window, "00001", "80's Rock", 
                                        "47 songs", "", "1:09:56")
            
            self._top_group._insert_a_row(self._a_row)

        if (self.props.name == "search_details"):
            
            self._more_results = self._set_group("Artist", False, False, self._overlayer)
            self._a_row = self._set_row(0, self._window, "UCmMUZbaYdNH0b", 
                                        "Sweet child of mine", "Guns N' Roses" , 
                                        "80's rock", "3:56")
            
            self._more_results._insert_a_row(self._a_row)

        if (self.props.name == "library_details"):
            
            self._details = self._set_details(self._overlayer, "Chico Buarque", "47")
            self._a_row = self._set_row(0, self._window, "FGghghghessdE", 
                                        "Com açucar, com afeto", "Chico Buarque" , 
                                        "MPB", "3:56")
            
            self._details._insert_a_row(self._a_row)