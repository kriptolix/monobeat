from gi.repository import Adw
from gi.repository import Gtk
from gi.repository import GObject

from .resultsrow import ResultsRow

@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/ResultsGroup.ui')
class ResultsGroup(Gtk.Box):
    
    __gtype_name__ = 'ResultsGroup'     

    _button_more = Gtk.Template.Child()
    _group_title = Gtk.Template.Child()
    _list_container = Gtk.Template.Child()

    def __init__(self, group_title, more, playable, handler):                     
        
        super().__init__()      

        self._group_title.props.label = group_title
        self._button_more.props.visible = more
        self._signal_target = handler

        if(playable):
            self._list_container.connect("row-activated", self._signal_target._on_row_played)
            self._list_container.connect("row-activated", self._signal_target._set_row_as_playing)
    
    def _insert_a_row(self, row):             
        
        self._list_container.append(row)

    
        
    
    
