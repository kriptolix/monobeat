from gi.repository import Adw
from gi.repository import Gtk
from gi.repository import GObject
from gi.repository import Gio


from enum import IntEnum
from .rowmenu import RowMenu


@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/ResultsRow.ui')
class ResultsRow(Adw.PreferencesRow):
    
    __gtype_name__ = 'ResultsRow'

    class Mode(IntEnum):
        """The different modes how a row can be showed
        """
        SEARCH_MUSIC_VIDEO = 0
        SEARCH_PLAYLIST_ALBUM = 1
        SEARCH_ARTIST = 2
        LIBRARY_MUSIC_VIDEO = 3
        LIBRARY_PLAYLIST_ALBUM = 4
        IN_PLAYLISTS_LIST = 5


    _check_button = Gtk.Template.Child()
    _drag_handle = Gtk.Template.Child()
    _play_indicator = Gtk.Template.Child()
    _item_title = Gtk.Template.Child()
    _secondary_info = Gtk.Template.Child()
    _tertiary_info = Gtk.Template.Child()
    _duration = Gtk.Template.Child()
    _menu_button = Gtk.Template.Child()
    _see_details = Gtk.Template.Child()


    mode = GObject.Property(type=int, default=Mode.SEARCH_MUSIC_VIDEO)
    reference = GObject.Property(type=str) # CONSTRUCT_ONLY 
    playing = GObject.Property(type=bool, default=False) #add notify to change icon visibility
    selection = GObject.Property(type=bool, default=False)

    def __init__(self, mode, handler, reference, item_title, 
                 secondary_info, tertiary_info, duration):

        '''
            handler is a reference to MainWindow who will deal with the 
            row button actions:
            see details, menu button (add/remove to/from playlist, play) 
        '''        
                
        super().__init__()

        self.props.mode = mode
        self._menu_button.props.popover = RowMenu()        

        self._handler = handler        
        self._action_entries = []

        self._setting_layout()        
        
        self._lenght = len(self._action_entries)

        if (self._lenght > 0):
            action_group = Gio.SimpleActionGroup()
            
            for name, callback in self._action_entries:
                action = Gio.SimpleAction.new(name, None)
                action.connect("activate", callback)
                action_group.add_action(action)

            self.insert_action_group("row", action_group)        

        self.props.reference = reference
        self._item_title.props.label = item_title
        self._secondary_info.props.label = secondary_info
        self._tertiary_info.props.label = tertiary_info
        self._duration.props.label = duration

        if (self._handler.props.name != "add_to_playlist"):
        
            self._see_details.connect("clicked", self._handler.
                                      _on_see_button_clicked)       
        
        
        #self.connect("notify::window", self._on_main_page_load)

        self.bind_property(
            "selection", self._check_button, "visible", 
            GObject.BindingFlags.BIDIRECTIONAL
            | GObject.BindingFlags.SYNC_CREATE)

        #self._check_button.connect("toggled", self._handler.on_check_button_toggled)
        
    
    def _play():
        print("action play")    
   
               
    def _setting_layout(self):        

        if (self.props.mode == ResultsRow.Mode.SEARCH_MUSIC_VIDEO):
            self._drag_handle.props.visible = False  
            self._see_details.props.visible = False         

            self._action_entries = [
            ("play", self._play),
            ("add_to_playlist", self._handler._on_add_to_playlist)
        ]           

        if (self.props.mode == ResultsRow.Mode.SEARCH_PLAYLIST_ALBUM):
            self._drag_handle.props.visible = False
            self._tertiary_info.props.visible = False
            #self._see_details.props.visible = False

            self._action_entries = [
            ("play", self._play),
            ("save_as_playlist", self._handler._on_add_to_playlist)
            ]
        
        if (self.props.mode == ResultsRow.Mode.SEARCH_ARTIST):
            self._drag_handle.props.visible = False
            self._play_indicator.props.visible = False
            self._secondary_info.props.visible = False
            self._tertiary_info.props.visible = False
            self._duration.props.visible = False
            self._menu_button.props.visible = False

        if (self.props.mode == ResultsRow.Mode.LIBRARY_MUSIC_VIDEO):
            self._drag_handle.props.visible = False
            self._see_details.props.visible = False

            self._action_entries = [
            ("play", self._play),
            ("add_to_playlist", self._handler._on_add_to_playlist),
            ("remove_from_playlist", self._handler._on_remove_from_playlist),
            ("selection_mode", self._selection_mode)
            ]
        
        if (self.props.mode == ResultsRow.Mode.LIBRARY_PLAYLIST_ALBUM):
            self._drag_handle.props.visible = False
            self._tertiary_info.props.visible = False

            self._action_entries = [
            ("play", self._play),
            ("add_to_playlist", self._handler._on_add_to_playlist),
            ("exclude_playlist", self._handler._on_exclude_playlist),
            ("rename_playlist", self._handler._on_rename_playlist)
            ]

        if (self.props.mode == ResultsRow.Mode.IN_PLAYLISTS_LIST):
            self.props.selection = True 
            self._drag_handle.props.visible = False
            self._play_indicator.props.visible = False
            self._secondary_info.props.visible = False
            self._tertiary_info.props.visible = False
            self._duration.props.visible = False
            self._menu_button.props.visible = False
            self._see_details.props.visible = False

    @Gtk.Template.Callback()
    def _on_row_hover(self, controller, x, y):   

        if (self._play_indicator.props.icon_name != "media-playback-start-symbolic"):
            self._play_indicator.props.icon_name = "media-playback-start-symbolic"

        #print("hover")
    
    @Gtk.Template.Callback()
    def _on_row_unhover(self, controller):
        
        if not (self.props.playing and 
            self._play_indicator.props.icon_name == "media-playback-start-symbolic"):
            self._play_indicator.props.icon_name = ""

        #print("unhover")    