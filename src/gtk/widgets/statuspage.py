from gi.repository import Adw
from gi.repository import Gtk

@Gtk.Template(resource_path='/io/gitlab/kriptolix/Monobeat/src/gtk/ui/StatusPage.ui')
class StatusPage(Adw.Bin):
    
    __gtype_name__ = 'StatusPage' 

    _status_page = Gtk.Template.Child()
    _spinner = Gtk.Template.Child()    
    
    def __init__(self):
                
        super().__init__()

        self.connect("notify::name", self._setup_section)


    def _setup_section(self, klass, param):

        if (self.props.name == "search_status"):
            self._status_page.props.title = "Search"
            self._status_page.props.icon_name = "loupe-large-symbolic"
        
        if (self.props.name == "library_status"):
            self._status_page.props.title = "Library"
            self._status_page.props.icon_name = "library-music-symbolic"
         

        